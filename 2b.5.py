import numpy as np

# Definerer koeffisientene og interceptet
intercept = -0.591661
coeff_skudd = 0.382565
coeff_corner = -0.100377

# Definerer de gitte verdiene for forklaringsvariablene
skudd_paa_maal_diff = 2
corner_diff = -2

# Beregner logodds
log_odds = (intercept + 
            coeff_skudd * skudd_paa_maal_diff + 
            coeff_corner * corner_diff)

# Konverterer logodds til sannsynlighet
probability_home_win = 1 / (1 + np.exp(-log_odds))
print(probability_home_win)